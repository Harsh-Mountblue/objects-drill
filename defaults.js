function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    newObj = {}
    for (k in defaultProps) {
        obj[k] = defaultProps[k];
    }
    return obj;
}

exports.defaults = defaults;