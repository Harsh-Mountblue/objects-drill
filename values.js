function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    arr=[]
    for (k in obj) {
        arr.push(obj[k]);
    }
    return arr;
}

exports.values = values;