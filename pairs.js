function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    arr=[]
    for (k in obj) {
        arr.push([k,obj[k]]);
    }
    return arr;
}

exports.pairs = pairs;