const temp = require('../defaults.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const defaultProps = { codeName: 'Batman', skills: 'Master tactician, Master fighter', worth: 'billionaire' };

console.log(temp.defaults(testObject, defaultProps));