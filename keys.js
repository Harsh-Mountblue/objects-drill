function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    arr=[]
    for (k in obj) {
        arr.push(k);
    }
    return arr;
}

exports.keys = keys;