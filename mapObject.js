function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    for (k in obj) {
        obj[k] = cb(obj[k]);
    }
    return obj;
}

exports.mapObject = mapObject;